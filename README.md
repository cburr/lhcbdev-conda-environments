# lhcbdev-conda-environments

Can we reliable overwrite existing environments?
Versioning environments?
Multiuser conda?
 * https://docs.conda.io/projects/conda/en/latest/user-guide/configuration/admin-multi-user-install.html
 * https://medium.com/@pjptech/installing-anaconda-for-multiple-users-650b2a6666c6

```bash
# This is safe to do from within the login scripts
#     * only provides a shell function called conda
#     * can also be used to create environment in $HOME/.conda (change to AFS work?)
source /cvmfs/lhcbdev.cern.ch/conda/etc/profile.d/conda.sh
source /cvmfs/lhcbdev.cern.ch/conda/etc/profile.d/conda.csh
source /cvmfs/lhcbdev.cern.ch/conda/etc/fish/conf.d/conda.fish

conda env create -f snakemake.yml --name snakemake
conda env export --file snakemake-deployed.yml
```
