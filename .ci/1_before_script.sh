#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

yum install -y s3fs-fuse
source /cvmfs/lhcbdev.cern.ch/conda-experimental/etc/profile.d/conda.sh
conda activate
ci_env_name=$(python -c 'import secrets; print(secrets.token_hex(10))')
conda create --quiet --yes --name "${ci_env_name}" python=3 conda-build git gitpython pyyaml requests
conda activate "${ci_env_name}"
