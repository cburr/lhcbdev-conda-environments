#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

source .ci/setup_git.sh
git add -f .concretised/
git status
code=$(git diff --cached --exit-code >&2 && echo 0 || echo $?)
if [ "${code}" -eq 1 ]; then
  git commit -m "Add concretised environments for deployment"
  git fetch --all
  git rebase origin/master
  git push origin HEAD:master
fi
