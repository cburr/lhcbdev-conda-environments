#!/usr/bin/env bash

# Set up pushing to GitLab using SSH
if [ -z "${GITLAB_SSH_KEY+x}" ]; then
    echo "Skipping git over SSH setup"
else
    echo "Setting up git over SSH"

    git config user.email "ci@${CI_PROJECT_PATH}"
    git config user.name "CI for ${CI_PROJECT_PATH}"

    GITLAB_SSH_WORKING_DIR="$(mktemp -d)"
    echo "${GITLAB_SSH_KEY}" >> "${GITLAB_SSH_WORKING_DIR}/id_rsa"
    chmod 600 "${GITLAB_SSH_WORKING_DIR}/id_rsa"
    echo 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i '"${GITLAB_SSH_WORKING_DIR}/id_rsa"' $*' > "${GITLAB_SSH_WORKING_DIR}/ssh"
    chmod +x "${GITLAB_SSH_WORKING_DIR}/ssh"
    export GIT_SSH="${GITLAB_SSH_WORKING_DIR}/ssh"
    git remote set-url origin "ssh://git@gitlab.cern.ch:7999/${CI_PROJECT_PATH}.git"
    git remote -v

    function cleanup_ssh {
        if [ -n "${GITLAB_SSH_WORKING_DIR+x}" ]; then
            rm -rf "${GITLAB_SSH_WORKING_DIR}"
        fi
    }
    trap cleanup_ssh EXIT
fi
