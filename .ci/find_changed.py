#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import os
from os.path import abspath, isdir, join, relpath
import re
import sys

import git

__all__ = [
    'get_changes',
]


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('repo_base')
    parser.add_argument('prefix')
    parser.add_argument('--depth', type=int, default=0, required=False)
    parser.add_argument('--regex', required=False)
    parser.add_argument('--merge-request', action='store_true')
    # parser.add_argument('--debug', action='store_true')
    args = parser.parse_args()

    assert not args.prefix.startswith('/')
    assert isdir(join(args.repo_base, args.prefix))

    changes = get_changes(args.repo_base, args.prefix, depth=args.depth,
                          regex=args.regex, merge_request=args.merge_request)
    print('\n'.join(changes))


def get_changes(repo_base, prefix, depth=0, regex=None, merge_request=False):
    repo_base = abspath(repo_base)
    prefix = relpath(abspath(prefix), repo_base)
    repo = git.Repo(repo_base)
    if regex:
        regex = re.compile(f'^{regex}$')

    # if os.environ['CI_COMMIT_REF_NAME'] == 'master':
    if merge_request:
        common_ancestor = repo.merge_base('HEAD', 'origin/master')
        assert len(common_ancestor) == 1, common_ancestor
        common_ancestor = common_ancestor[0]
        diffs = common_ancestor.diff('HEAD')
    else:
        diffs = repo.commit('HEAD~1').diff('HEAD')
    # sys.stderr.write(f'Changes are: {[(d.a_path, d.b_path, d.new_file, d.renamed_file, d.deleted_file)for d in diffs]}\n')

    changes = set()
    for diff in diffs:
        if diff.deleted_file:
            continue
        full_path = diff.b_path
        if full_path is not None and full_path.startswith(prefix):
            full_path = relpath(full_path, prefix)
            result = full_path
            if depth:
                result = join(*full_path.split(os.sep)[:depth])
            if regex:
                match = regex.match(full_path)
                # sys.stderr.write(f'Pattern "{regex.pattern}" matches {full_path} with {match}\n')
                if not match:
                    continue
            changes.add(result)

    return sorted(changes)


if __name__ == '__main__':
    parse_args()
