#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

if [ "$#" -ne 1 ]; then
  echo "Usage: .ci/test_lbconda_environment.sh path/to/environment.yml"
  exit 41
fi

CI_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
ENVIRONMENT_YML_FN=$1
if [ ! -f "${ENVIRONMENT_YML_FN}" ]; then
  echo "ERROR: ${ENVIRONMENT_YML_FN} does not exist"
  exit 42
fi


WORKING_DIR="$(mktemp -d)"
echo "Working directory is ${WORKING_DIR}"
CONDA_MIRROR_DIR="${WORKING_DIR}/mirror"
mkdir "${CONDA_MIRROR_DIR}"
function cleanup_mirror {
    if [ -z "${CONDA_S3_MOUNTED+x}" ]; then
        if [ -z "${LBCONDA_NO_CLEAN_WORKING_DIR+x}" ]; then
            rm -rf "${WORKING_DIR}"
        fi
    else
        rm "${WORKING_DIR}/.aws-credentials"
        umount "${CONDA_MIRROR_DIR}"
    fi
}
trap cleanup_mirror EXIT

if [ -z ${S3_ACCESS_KEY_ID+x} ] || [ -z ${S3_SECRET_ACCESS_KEY+x} ] || [ -z ${S3_BUCKET_NAME+x} ]; then
    echo "Running in standalong mode"
else
    echo "Running with s3 credentials"
    CONDA_S3_MOUNTED=1

    # s3fs
    echo "${S3_ACCESS_KEY_ID}:${S3_SECRET_ACCESS_KEY}" > "${WORKING_DIR}/.aws-credentials"
    chmod 600 "${WORKING_DIR}/.aws-credentials"

    unset S3_ACCESS_KEY_ID
    unset S3_SECRET_ACCESS_KEY

    s3fs "${S3_BUCKET_NAME}" "${CONDA_MIRROR_DIR}" \
        -o url="https://s3.cern.ch" \
        -o passwd_file="${WORKING_DIR}/.aws-credentials" \
        -o use_path_request_style

    BUCKET_URL=https://${S3_BUCKET_NAME}.s3.cern.ch
fi

"${CI_DIR}/concretise-environment.py" \
    "${ENVIRONMENT_YML_FN}" \
    "${CONDA_MIRROR_DIR}" \
    --bucket-url="${BUCKET_URL+${BUCKET_URL}}"
