#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

changes=$(.ci/find_changed.py "${PWD}" environments/ --regex '.+\.yml')
echo "Detected changed environments ${changes}"

for fn in ${changes}; do
  echo "Running for ${fn}"
  .ci/concretise-environment.sh "environments/${fn}"
done

git status
