#!/usr/bin/env python3
import argparse
from datetime import datetime
import hashlib
import os
from os.path import abspath, basename, dirname, isdir, isfile, join, relpath
import re
from subprocess import check_call, check_output
import tempfile

import requests
import yaml


def mirror_package(url, md5_hash, mirror_path):
    arch_type = basename(dirname(url))
    filename = join(mirror_path, arch_type, basename(url))
    os.makedirs(dirname(filename), exist_ok=True)
    if isfile(filename):
        print('Found', url, 'in cache as', filename)
        with open(filename, 'rb') as fp:
            file_hash = hashlib.md5(fp.read()).hexdigest()
        if file_hash != md5_hash:
            raise NotImplementedError(file_hash, md5_hash)
    else:
        print('Mirroring', url, 'to', filename)
        response = requests.get(url, allow_redirects=True)
        assert response.ok, response
        url_hash = hashlib.md5(response.content).hexdigest()
        if url_hash != md5_hash:
            raise NotImplementedError(url_hash, md5_hash)
        with open(filename, 'wb') as fp:
            fp.write(response.content)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('environment_yml_fn')
    parser.add_argument('mirror_path')
    parser.add_argument('--bucket-url', required=False)
    args = parser.parse_args()
    concretise_environment(args.environment_yml_fn, args.mirror_path,
                           bucket_url=args.bucket_url)


def concretise_environment(env_yml_fn, mirror_path, bucket_url=None):
    env_yml_fn = abspath(env_yml_fn)
    repo_dir = dirname(dirname(__file__))
    envs_dir = join(repo_dir, 'environments')
    if env_yml_fn.startswith(f'{envs_dir}{os.sep}'):
        env_name = relpath(env_yml_fn, start=envs_dir)
        output_dir = join(repo_dir, '.concretised', env_name)
    else:
        env_name = basename(env_yml_fn)
        output_dir = dirname(env_yml_fn)

    # Input validation
    if not env_name.endswith('.yml'):
        raise ValueError('Environment definition filename must end with .yml')
    env_name = env_name[:-len('.yml')]

    banned_chars = ('-', ' ', ':', '#')
    if any(c in env_name for c in banned_chars):
        raise ValueError(f'Environment name cannot contain any of {banned_chars}')
    env_name = env_name.replace('/', '-')

    if not isfile(env_yml_fn):
        raise FileNotFoundError(env_yml_fn)

    if not isdir(mirror_path):
        raise FileNotFoundError(mirror_path)

    # Create a dummy environment to concretise it
    with tempfile.TemporaryDirectory() as tmp_dir:
        prefix = join(tmp_dir, basename(env_yml_fn))

        command = ['conda', 'env', 'create', '--prefix', prefix, '--file', env_yml_fn, '--quiet']
        print('Creating environment with', command)
        check_call(command)

        command = ['conda', 'list', '--prefix', prefix, '--explicit', '--md5']
        print('Getting package URL list with', command)
        list_output = check_output(command)

        command = ['conda', 'env', 'export', '--prefix', prefix]
        print('Exporting environment to yaml with', command)
        env_output_raw = check_output(command)

    # Parse the output of conda list --explicit --md5
    urls = [l.split('#')
            for l in list_output.decode().split('\n')
            if re.match(r'[^#]', l)]
    assert urls[0] == ['@EXPLICIT'], urls[0]
    urls = {url: md5_hash for url, md5_hash in urls[1:]}

    # Parse the output of conda env export
    env_output = yaml.load(env_output_raw)
    del env_output['name']
    del env_output['prefix']
    del env_output['channels']
    assert len(env_output) == 1, env_output.keys()
    pip_dependencies = []
    conda_dependencies = []
    for dep in env_output['dependencies']:
        if isinstance(dep, dict):
            assert len(dep) == 1, dep
            pip_dependencies.extend(dep['pip'])
        elif isinstance(dep, str):
            name, version, build_string = dep.split('=')
            filename = f'{name}-{version}-{build_string}'
            for url, md5_hash in urls.items():
                if basename(url).startswith(filename):
                    break
            else:
                raise ValueError(f'Failed to find URL for {filename}')
            mirror_package(url, md5_hash, mirror_path)
            # conda_dependencies.append(mirrored_url)
            conda_dependencies.append(dep)
        else:
            raise NotImplementedError(type(dep))
    assert len(conda_dependencies) == len(urls)

    check_call(['conda', 'index', '--no-progress', '--verbose',
                '--channel-name', bucket_url or 'unknown-channel',
                '--threads', str(8), mirror_path])

    # Generate the output yaml
    rendered_env = {
        'name': env_name,
        'channels': [bucket_url or f'file://{mirror_path}'],
        'dependencies': conda_dependencies+[{'pip': pip_dependencies}],
    }

    rendered_env_fn = f'{datetime.utcnow().isoformat()}.final.yml'
    rendered_env_fn = join(output_dir, rendered_env_fn)

    os.makedirs(output_dir, exist_ok=True)
    with open(rendered_env_fn, 'wt') as fp:
        yaml.dump(rendered_env, fp)

    print('Environment can be created with:')
    print(f'    conda env create --name {env_name} --file {rendered_env_fn}')


if __name__ == '__main__':
    parse_args()
